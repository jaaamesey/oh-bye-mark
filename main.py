"""MAIN"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QLabel)
#from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtGui import QPixmap

# import scipy
# import skimage # Keep last

if __name__ == '__main__':
    APP = QApplication(sys.argv)
    W = QWidget()

    #BUTTONHBOX = QHBoxLayout(W)
    BUTTON = QPushButton("Remove Watermark", W)
    LABEL = QLabel(W)
    PIXMAP = QPixmap('images.jpg')
    LABEL.setPixmap(PIXMAP)

    W.resize(400, 250)
    W.setWindowTitle('OhByeMark Watermark Emancipator')
    W.show()
    sys.exit(APP.exec_())
